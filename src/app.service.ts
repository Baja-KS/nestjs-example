import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    // Capitalized comment
    // var something;
    const someArray = ['m1', 'm2'];
    return 'Hello World!';
  }
}
