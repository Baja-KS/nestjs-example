import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {SwaggerModule} from '@nestjs/swagger';
import {generateSwaggerDoc} from './swagger';


/** JDOC **/
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  SwaggerModule.setup("docs",app,generateSwaggerDoc(app));

  await app.listen(3000);
}
bootstrap();
