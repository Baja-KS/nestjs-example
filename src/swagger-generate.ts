import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {stringify} from 'yaml';
import * as fs from 'fs';
import {generateSwaggerDoc} from './swagger';



async function makeSwaggerFile() {
  const app = await NestFactory.create(AppModule);
  const document = generateSwaggerDoc(app);
  const swaggerYaml = stringify(document,{});
  const swaggerJson = JSON.stringify(document,null,4);
  fs.writeFileSync('./openapi.yaml',swaggerYaml);
  fs.writeFileSync('./openapi.json',swaggerJson);
  await app.close();
}
makeSwaggerFile();
