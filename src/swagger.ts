import {INestApplication} from '@nestjs/common';
import {DocumentBuilder, OpenAPIObject, SwaggerModule} from '@nestjs/swagger';

export function generateSwaggerDoc(app:INestApplication):OpenAPIObject{
  const config = new DocumentBuilder()
    .setTitle("NestJS example app")
    .setDescription("Example of a NestJS api")
    .setVersion("0.0.1")
    .addTag("example")
    .build();

  return SwaggerModule.createDocument(app,config);
}
